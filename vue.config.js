const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  devServer: {
    port: 20024,
    // allowedHosts: [
    //   ".top"
    // ],
    // proxy: {
    //   '^/api': {
    //     target: 'https://api.github.com',
    //     pathRewrite: {'^/api': ''},
    //     changeOrigin: true
    //   }
    // }
  }
})
