// import {Button, message,Tag,Layout,Menu,Dropdown,Icon,Upload,Input} from 'ant-design-vue';
import {message,Spin,Table, notification,Slider,Row,Col,InputNumber} from 'ant-design-vue';
const ant = {
    install: function (Vue) {
        // Vue.component(Button.name, Button,Tag,Layout,Menu,Dropdown,Icon,Upload,Input);
        message.config({
            top: `100px`,
        });
        
        Vue.use(message);
        Vue.use(Slider);
        Vue.use(Spin);
        Vue.use(Table);
        Vue.use(notification);
        Vue.use(Row);
        Vue.use(Col);
        Vue.use(InputNumber);
        // Vue.use(Dropdown)
        // Vue.use(Tag)
        Vue.prototype.$message = message;
        Vue.prototype.$notification = notification
    }
};
export default ant