import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from "../views/Main.vue"
import Analysis from "../views/Analysis.vue"
import Integration from "../views/Integration.vue"
import Predict from "../views/Predict.vue"
import Imputation from "../views/Imputation.vue"
import TaskList from "../views/TaskList.vue"
import PredictionResults from "../results/PredictionResults.vue"
import IntegrationResults from "../results/IntegrationResults.vue"
import ImputationResults from "../results/ImputationResults.vue"
import News from "../views/News.vue"
import HomePage from "../views/HomePage.vue"
import Guidance from "../views/Guidance.vue"
// import LeidenClusterComponent from "../components/LeidenClusterComponent.vue"
// import LouvainClusterComponent from "../components/LouvainClusterComponent.vue"
// import KmeansClusterComponent from "../components/KmeansClusterComponent.vue"
// import HierarchicalClusterComponent from "../components/HierarchicalClusterComponent.vue"
Vue.use(VueRouter)

const router =  new VueRouter({
    routes:[
     
        {
            path: "/",
            redirect: "/main/home"
        },
        {
            name:"main",
            path: "/main",
            component: Main,
            children:[
                {
                    name: "news",
                    path: "news",
                    component: News
                },
                {
                    name: "guidance",
                    path: "guidance",
                    component: Guidance
                },
                {
                    name: "home",
                    path: "home",
                    component: HomePage
                }
            ]
        },
        {
            name: "analysis",
            path: "/analysis",
            component: Analysis,
            children: [
                {
                    name: "predict",
                    path: "predict",
                    component: Predict
                },
                {
                    name: "integration",
                    path: "integration",
                    component: Integration,
                },
                {
                    name: "imputation",
                    path: "imputation",
                    component: Imputation
                },
                {
                    name: "taskList",
                    path: "taskList",
                    component: TaskList
                },
                {
                    name: "predictionResults",
                    path: "predictionResults",
                    component: PredictionResults
                },
                {
                    name: "integrationResults",
                    path: "integrationResults",
                    component: IntegrationResults,
                    // children:
                    // [
                    //     {name: "leidenClusterComponent", path: "leidenClusterComponent", component: LeidenClusterComponent},
                    //     {name: "louvainClusterComponent", path: "louvainClusterComponent", component: LouvainClusterComponent},
                    //     {name: "kmeansClusterComponent", path: "kmeansClusterComponent", component: KmeansClusterComponent},
                    //     {name: "hierarchicalClusterComponent", path: "hierarchicalClusterComponent", component: HierarchicalClusterComponent}
                    // ]
                },{
                    name: "imputationResults",
                    path: "imputationResults",
                    component: ImputationResults
                }
            ]
        },


    ]
})

export default router