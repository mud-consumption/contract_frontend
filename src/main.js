import Vue from 'vue'
import App from './App.vue'
import router from './router/index'
import VueRouter from 'vue-router'

import 'admin-lte/dist/css/adminlte.css'
import 'admin-lte/dist/js/adminlte.js'
import 'admin-lte/plugins/fontawesome-free/css/all.min.css'
import ant from "./config/AntDesign";

Vue.config.productionTip = false
Vue.use(ant)
Vue.use(VueRouter)

new Vue({
  render: h => h(App),
  beforeCreate() {
		Vue.prototype.$bus = this //安装全局事件总线
	},
  router
}).$mount('#app')
