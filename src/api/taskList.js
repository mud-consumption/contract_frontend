import requests from "./request";


// 下载测试文件）
export function fileDownload(taskId){
    return requests({
        url: "/resultDownload",
        method: "get",
        // responseType: 'blob',
        // timeout: 7000,
        params: {
            taskId: taskId
        }
    })
}

export function getIntegrationResult(params){
    return requests({
        url: "/getIntegrationResult",
        method: "get",
        params: params, 
        timeout: 7000
    })
}

export function getImputationResult(params){
    return requests({
        url: "/getImputationResult",
        method: "get",
        params: params,
        timeout: 7000
    })
}

export function getPredictionResult(params){
    return requests({
        url: "/getPredictionResult",
        method: "get",
        params: params,
        // timeout: 70000
    })
}

export function getAllTaskList(params){
    return requests({
        url: "/getTaskList",
        method: "get",
        params: params,
        timeout: 9000,
    })
}

// 上传integration任务
export function uploadIntegration(fileObj, clusterParam){
    const file = new FormData()
    
    
    let ATACFile = fileObj.ATACFile
    let RNAFile = fileObj.RNAFile

    let ATACRef = fileObj.ATACRef
    let RNARef = fileObj.RNARef    

    if (ATACRef.length != 0){        
        ATACRef = ATACRef[0]
    } 

    if (RNARef.length != 0){
        RNARef = RNARef[0]
    }
    
    if (ATACFile.length != 0){
        ATACFile = ATACFile[0]
    }

    if (RNAFile.length != 0){
        RNAFile = RNAFile[0]
    }


    file.append("ATACFile", ATACFile)
    file.append("RNAFile", RNAFile)
    file.append("ATACRef", ATACRef)
    file.append("RNARef", RNARef)

    return requests({
        method: "post",
        url: "/taskUploadIntegration",
        data: file,
        params: clusterParam
    })
}

// 上传文件
export function uploadFile(fileObj, url){
    const file = new FormData()
    
    
    let ATACFile = fileObj.ATACFile
    let RNAFile = fileObj.RNAFile

    let ATACRef = fileObj.ATACRef
    let RNARef = fileObj.RNARef    

    if (ATACRef.length != 0){        
        ATACRef = ATACRef[0]
    } 

    if (RNARef.length != 0){
        RNARef = RNARef[0]
    }
    
    if (ATACFile.length != 0){
        ATACFile = ATACFile[0]
    }

    if (RNAFile.length != 0){
        RNAFile = RNAFile[0]
    }


    file.append("ATACFile", ATACFile)
    file.append("RNAFile", RNAFile)
    file.append("ATACRef", ATACRef)
    file.append("RNARef", RNARef)

    return requests({
        method: "post",
        url: url,
        data: file,
        headers: {
          'Content-Type': 'multipart/form-data', // 添加 Content-Type 头部
        }
      })
  }