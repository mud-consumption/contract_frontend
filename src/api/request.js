import axios from 'axios'
// 创建axios实例
const requests = axios.create({
    // withCredentials: true,
    // baseURL: "https://icaupback.cpolar.cn"
    baseURL: "http://localhost:20025"
    // timeout: 7000 // 设置统一的超时时长
})


// 配置请求拦截器
requests.interceptors.request.use((config)=>{
    return config
})


requests.interceptors.response.use(
    // 成功的回调
    (res)=>{
        return res
    }, 
    // 失败的回调
    (err)=>{
        return Promise.reject(new Error("failed"))
    }
)

export default requests