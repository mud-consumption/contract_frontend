import * as echarts from 'echarts';
import 'echarts-gl'
import { produceRandomColor } from "./utils";
import dataFirst from '../assets/data.json'

// 绘制2D散点图形
export function scatterPlot(dom, data, joint_vis=false){
    if (!data){
      data = dataFirst
    }
      let MyChart =  echarts.init(dom)
  
      let visualX = []
      let visualY = []
      let cellType =[]
      let seriesData = []
    
  
      // 把JSON格式的数据提取出来
      if (!joint_vis){
          data.forEach((item)=>{
            visualX.push(item.visualize_x)
            visualY.push(item.visualize_y)
            cellType.push(item.cell_type)
            seriesData.push([item.visualize_x, item.visualize_y])
        })
        
      } else{
        data.forEach((item)=>{
          visualX.push(item.visualize_x)
          visualY.push(item.visualize_y)
          cellType.push(item.domain)
          seriesData.push([item.visualize_x, item.visualize_y])
        })
      }
    
      
      let labels = Array.from(new Set(cellType))
     
  
      let CLUSTER_COUNT = labels.length
      let dataGroup = []
      for (let i=0;i < CLUSTER_COUNT;i ++){
        dataGroup.push([])
      }


      // 数据按照细胞类型分组
      for(let i=0;i < seriesData.length;i ++){
        let cell = cellType[i]
        dataGroup[labels.indexOf(cell)].push(seriesData[i])
      }

   
  
    // 准备颜色
      let COLOR_ALL = ['#DB5F57', "#57D3DB"]
        if (CLUSTER_COUNT > COLOR_ALL.length){
            for (let i = COLOR_ALL.length;i < CLUSTER_COUNT.length;i ++){
                COLOR_ALL.push(produceRandomColor())
            }
        }
  
      let option = {
        toolbox:{
          show: true,
          feature:{
            dataView: { readOnly: false },
            saveAsImage: {type: "png"}
          }
        },
          tooltip: {
              axisPointer: {
                type: 'cross',
                crossStyle: {
                  color: '#999'
                }
              },
            formatter(param){
              let data = param.data
              return [
                "<strong>V_X</strong>: " + data[0] + "<br/>",
                "<strong>V_Y</strong>: " + data[1] + "<br/>",
                "<strong>Cell Type</strong>:" + param.seriesName + "<br/>"
              ].join('')
            }
          },
         
          xAxis: {
              name: "Dimention 1",
              nameLocation: 'middle',
              nameGap:30,
              axisLine:{
                  onZero: false
              },
              nameTextStyle: {
                  fontWeight: "bold",
                  fontFamily: "Times New Roman",
                  color:"black",
                  fontSize:16,
              },
              axisLabel:{
                  fontWeight:'bold',
                  fontFamily: "Times New Roman",
                  color:"black",
                fontSize:16,
              }
          },
          yAxis: {
              name: "Dimention 2",
              nameLocation: 'middle',
              nameGap:30,
              axisLine:{
                  onZero: false
              },
              nameTextStyle: {
                fontWeight: "bold",
                fontFamily: "Times New Roman",             
                color:"black",
                fontSize:16,
  
              },
              axisLabel:{
                  fontWeight:'bold',
                  fontFamily: "Times New Roman",
                  color:"black",
                fontSize:16,
              }
          },
          
          series: [],
          legend: {
            type: "scroll",
            itemHeight: 8,
            orient: "horizontal",
            align: "left",  
            // right: "5%",
            top: 'bottom',
            width: "80%",
            textStyle:{
              fontFamily: "Times New Roman"
            }
            
          },
          grid: {
            bottom: 100
          },
        };
        
        // 填充数据
        for (let i = 0;i < dataGroup.length; i ++){
          option.series.push({
            type: "scatter",
            name: labels[i],
            symbolSize: 1.5,
            data: dataGroup[i],
            color: COLOR_ALL[i]
          })
        }
        MyChart.setOption(option);
        window.addEventListener('resize',function(){
          MyChart.resize();
        });
}

// 绘制预测柱状图
export function auROC_plot(dom, data){
    var colors = ['#c1232b', '#27727b', '#fcce10', '#e87c25', '#b5c334'];
  
    let myChart = echarts.init(dom);
  
    let metrics = [data.Accuracy, data.auROC, data.pr_auc, data.MCC, data.Sensitivity]
    var option = {
      toolbox:{
        show: true,
        feature:{
          dataView: { readOnly: false },
          saveAsImage: {type: "png"}
        }
      },
      tooltip: {
        trigger: "axis",
        axisPointer: {
          // 坐标轴指示器，坐标轴触发有效
          type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
        }
      },
  
      xAxis: {
        type: 'category',
        data: ['Accuracy', 'auROC', 'PR-AUC', 'MCC', 'Sensitivity'],
        axisLabel: {
          fontWeight: "bold",
          fontFamily: "Times New Roman",
          color: "black",
          fontSize: 16,
          rotate: 30,
        }
      },
      yAxis: {
        type: 'value',
        axisLabel: {
          fontWeight: "bold",
          fontFamily: "Times New Roman",
          color: "black",
          fontSize: 16,
        }
      },
      series: [{
        data: metrics,
        type: 'bar',
        itemStyle: {
          color: function(params) {
              // 设置每个柱子的颜色
              return colors[params.dataIndex]
          }
        }
      }],
     
  
    };
    myChart.setOption(option) 
  
}

// 绘制聚类柱状图
export function cluster_bar_plot(dom, data){
    let MyChart =  echarts.init(dom)

    let ami = data.ami
    let ari = data.ari
    let silhouette_score = data.silhouette_score
    let v_score = data.v_score
    let option = {
      toolbox:{
        show: true,
        feature:{
          dataView: { readOnly: false },
          saveAsImage: {type: "png"}
        }
      },
      tooltip: {
        trigger: "axis",
        axisPointer: {
          // 坐标轴指示器，坐标轴触发有效
          type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
        }
      },
      xAxis: [
        {
          type: "category",
          data: ["AMI", "AMI", "S-Score", "V-Score"],
          // axisTick: {
          //   alignWithLabel: true
          // }
         
          textStyle:{
            fontFamily: "Times New Roman"
          },
          nameTextStyle: {
            fontWeight: "bold",
            fontFamily: "Times New Roman"
          },
          axisLabel:{
              fontWeight:'bold',
              fontFamily: "Times New Roman",
              color:"black",
              fontSize:16,
              rotate:45
          }
        }
      ],
      yAxis: [
        {
          type: "value",
          nameTextStyle: {
            fontWeight: "bold",
            fontFamily: "Times New Roman"
          },
          axisLabel:{
              fontWeight:'bold',
              fontFamily: "Times New Roman",
              color:"black",
              fontSize:16
          }
        },
        
        
      ],
      series: [
        {
         
          type: "bar",
          barWidth: "60%",
          data: [ami,ari,silhouette_score,v_score],
          itemStyle: {
            color: function(params) {
                // 设置每个柱子的颜色
                var colorList = [
                  '#e01f54', '#001852', '#f5e8c8', '#b8d2c7', '#c6b38e'
                ];
                return colorList[params.dataIndex]
            }
        },
        }
      ],
  
      
    };  
      MyChart.setOption(option);
      window.addEventListener('resize',function(){
        MyChart.resize();
      });
}

// 损失函数绘制
export function loss_plot(dom, data){  
  if (data.length > 10){
    data = data.slice(0, -2)
  }
  
  let myChart = echarts.init(dom);
  let xData = []
  for (var i = 0;i < data.length;i ++){
    xData.push(i)
  }
  let option = {
    toolbox:{
      show: true,
      feature:{ 
        dataView: { readOnly: false },
        saveAsImage: {type: "png"}
      }
    },
    tooltip: {
      trigger: "axis",
      axisPointer: {
        // 坐标轴指示器，坐标轴触发有效
        type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
      }
    },
    xAxis: {
      name: "Epochs",
      nameLocation: 'middle',
      nameGap:30,
      type: 'category',
      data: xData,
      axisLabel:{
        fontWeight:'bold',
        fontFamily: "Times New Roman",
        color:"black",
        fontSize:16,
      },
      nameTextStyle: {
        fontWeight: "bold",
        fontFamily: "Times New Roman",
        color: "black",
        fontSize: 16
      },
    },
    yAxis: {
      name: "Loss Value",
      nameLocation: 'middle',
      nameGap:30,
      type: 'value',
      nameTextStyle: {
        fontWeight: "bold",
        fontFamily: "Times New Roman",
        color: "black",
        fontSize: 16
      },
      axisLabel:{
        fontWeight:'bold',
        fontFamily: "Times New Roman",
        color:"black",
        fontSize:16,
      },
    },
    series: [
      {
        data: data,
        type: 'line',
        smooth: true
      }
    ]
  };
  myChart.setOption(option) 
}