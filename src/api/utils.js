import moment from 'moment';
import 'moment-timezone';

// 随机生成颜色
export function produceRandomColor(){
    let colorStr="";
    let randomArr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f']
    for (let i = 0;i<6;i++){
        colorStr+=randomArr[Math.ceil(Math.random()*15)]
    }
    return '#'+colorStr
}

// 修改时间格式
export function timeFormat(timestamp_str){
    if (timestamp_str === null || typeof timestamp_str === 'undefined') {
        return null;
      }
    
      const formattedDateTime = moment(timestamp_str).tz('Asia/Shanghai').format('YYYY-MM-DD HH:mm:ss');
      return formattedDateTime;
}


  